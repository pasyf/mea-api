import Joi from '@hapi/joi';
import {data} from './data';

export const dataArray = Joi.array()
    .items(data)
    .label('Array of Data');
