import Joi from '@hapi/joi';
import JoiDate from '@hapi/joi-date';
Joi.extend(JoiDate);

export const data = Joi.object({
    id: Joi.string()
        .uuid()
        .description('the data id'),
    name: Joi.string()
        .max(255)
        .required()
        .description('the data name'),
    value: Joi.string()
        .max(255)
        .required()
        .description('the data value'),
    createdAt: Joi.date().description('the data created at'),
    updatedAt: Joi.date().description('the data updated at')
}).label('Data');
