import {DataModel} from './models';

const fetchDatas = (offset, limit) => {
    const options = {offset, limit};
    return DataModel.findAndCountAll(options);
};

const fetchDataById = id => {
    return DataModel.findByPk(id);
};

const createData = data => {
    return DataModel.create(data);
};

const updateData = (id, data) => {
    const options = {where: {id}};
    return DataModel.update(data, options);
};

const deleteData = id => {
    const options = {where: {id}};
    return DataModel.destroy(options);
};

export default {
    fetchDatas,
    fetchDataById,
    createData,
    updateData,
    deleteData
};
