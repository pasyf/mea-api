import handler from './handler';
import {pagination, uuid} from '../common/validators';
import {data, dataArray} from './validators';

export const routes = [
    {
        method: 'GET',
        path: '/datas',
        options: {
            handler: handler.fetchDatas,
            description: 'Get datas',
            notes: 'Returns all datas',
            tags: ['api', 'data'],
            validate: {
                query: pagination
            },
            response: {
                status: {
                    200: dataArray,
                    400: undefined,
                    404: undefined,
                    500: undefined
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/datas/{id}',
        options: {
            handler: handler.fetchDataById,
            description: 'Get data by id',
            notes: 'Returns data by id',
            tags: ['api', 'data'],
            validate: {
                params: uuid
            },
            response: {
                status: {
                    200: data,
                    400: undefined,
                    404: undefined,
                    500: undefined
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/datas',
        options: {
            handler: handler.createData,
            description: 'Create data',
            notes: 'Creates a new data',
            tags: ['api', 'data'],
            validate: {
                payload: data
            },
            response: {
                status: {
                    201: undefined,
                    400: undefined,
                    409: undefined,
                    500: undefined
                }
            }
        }
    },
    {
        method: 'PATCH',
        path: '/datas/{id}',
        options: {
            handler: handler.updateData,
            description: 'Update data by id',
            notes: 'Updates a data by id',
            tags: ['api', 'data'],
            validate: {
                params: uuid,
                payload: data
            },
            response: {
                status: {
                    204: undefined,
                    400: undefined,
                    500: undefined
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/datas/{id}',
        options: {
            handler: handler.deleteData,
            description: 'Delete data by id',
            notes: 'Deletes a data by id',
            tags: ['api', 'data'],
            validate: {
                params: uuid
            },
            response: {
                status: {
                    204: undefined,
                    400: undefined,
                    500: undefined
                }
            }
        }
    }
];
