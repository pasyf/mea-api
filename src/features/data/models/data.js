import {STRING, UUID, UUIDV4} from 'sequelize';
import {database} from '../../../core';

export const DataModel = database.define(
    'Data',
    {
        id: {
            type: UUID,
            defaultValue: UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: STRING(255),
            allowNull: false
        },
        value: {
            type: STRING(255),
            allowNull: false
        }
    },
    {
        tableName: 'data'
    }
);
