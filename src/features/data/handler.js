import {Boom} from '@hapi/boom';
import dataController from './controller';

const fetchDatas = (req, h) => {
    const {offset, limit} = req.query;
    return dataController
        .fetchDatas(offset, limit)
        .then(data => h.paginate(data.rows, data.count))
        .catch(() => Boom.notFound());
};

const fetchDataById = (req, h) => {
    const {id} = req.params;
    return dataController
        .fetchDataById(id)
        .then(data => h.response(data).code(200))
        .catch(() => Boom.notFound());
};

const createData = (req, h) => {
    const {payload} = req;
    return dataController
        .createData(payload)
        .then(() => h.response().code(201))
        .catch(() => Boom.notFound());
};

const updateData = (req, h) => {
    const {id} = req.params;
    const {payload} = req;
    return dataController
        .updateData(id, payload)
        .then(() => h.response().code(204));
};

const deleteData = (req, h) => {
    const {id} = req.params;
    return dataController.deleteData(id).then(() => h.response().code(204));
};

export default {
    fetchDatas,
    fetchDataById,
    createData,
    updateData,
    deleteData
};
