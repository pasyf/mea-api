import Joi from '@hapi/joi';

export const pagination = Joi.object({
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    pagination: Joi.boolean()
}).label('Pagination');
