import Joi from '@hapi/joi';

export const uuid = Joi.object({
    id: Joi.string()
        .uuid()
        .description('the uuid')
}).label('UUID');
