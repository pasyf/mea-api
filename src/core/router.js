import {routes as dataRoutes} from '../features/data';
import {routes as teapotRoutes} from '../features/teapot';

export const routes = [...dataRoutes, ...teapotRoutes];
