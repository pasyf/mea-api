import 'regenerator-runtime/runtime';
import Lab from '@hapi/lab';
import Code from '@hapi/code';
import {createServer} from '../../../dist/core/server';

const {afterEach, before, describe, it} = exports.lab = Lab.script();
const {expect} = Code;

describe('GET /teapot', () => {
    let server;

    before(async (done) => {
        server = await createServer();
        await server.start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 418', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/teapot'
        });
        expect(res.statusCode).to.equal(418);
    });
});
